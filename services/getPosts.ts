export const getAllPosts = async () => {
  const response = await fetch(process.env.NEXT_PUBLIC_ENV_BASE_URL);

  if (!response.ok) throw new Error("Unable to fetch posts.");

  return response.json();
};

export const getPostsBySearch = async (search: string) => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_ENV_BASE_URL}?q=${search}`
  );

  if (!response.ok) throw new Error("Unable to fetch posts.");

  return response.json();
};
