import Link from "next/link";

const TheHeader = () => {
  return (
    <>
      <h2>{process.env.NEXT_PUBLIC_ENV_TEXT}</h2>
      <header>
        <Link href="/">Home</Link>
        <Link href="/blog">Blog</Link>
        <Link href="/about">About</Link>
      </header>
    </>
  );
};

export { TheHeader };
